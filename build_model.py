from keras.applications.mobilenet import MobileNet
from keras.applications.inception_v3 import InceptionV3
from keras.optimizers import SGD, Adadelta, Adam
from keras.preprocessing.image import ImageDataGenerator
from keras_squeezenet import SqueezeNet
from keras.callbacks import EarlyStopping, ModelCheckpoint
import time
import argparse


# USE KERAS 2.2.0 or current SqueezeNet implementation won't work


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("train_dir",
                        action="store",
                        help="Directory for training data containing subdirs split by label")
    parser.add_argument("test_dir",
                        action="store",
                        help="Directory for test data containing subdirs split by label")
    parser.add_argument("-e", "--epochs",
                        action="store",
                        type=int,
                        default=10,
                        help="Number of epochs (default=10)")
    parser.add_argument("-b", "--batches",
                        action="store",
                        type=int,
                        default=32,
                        help="Batch size (default=32)")
    parser.add_argument("--aug",
                        action="store_true",
                        help="Enable data augmentation")
    parser.add_argument("-m", "--model",
                        action="store",
                        dest="model",
                        help="Model to be used: 'squeezeNet' (default), 'mobileNet', 'inceptionV3'",
                        default="squeezeNet",
                        type=str)
    parser.add_argument("-o", "--opt",
                        dest="optimizer",
                        help="Optimizer to be used: 'Adadelta'(default), 'SGD', 'Adam'",
                        default="Adadelta",
                        type=str)
    parser.add_argument("-c", "--classes",
                        dest="n_classes",
                        help="Number of classes to be taken into account, 2 (default) or 3",
                        default=2,
                        type=int)
    args = parser.parse_args()
    train_dir = args.train_dir
    test_dir = args.test_dir
    epochs = args.epochs
    batch = args.batches
    model_name = args.model
    optimizer_name = args.optimizer
    aug = False
    if args.aug:
        aug = True

    train(train_dir, test_dir, epochs, batch, model_name, optimizer_name, aug, args.n_classes)


def train(train_dir, test_dir, epochs, batch, model_name, optimizer_name, aug, n_classes, model_id=None):
    """
    Train a Convolutional Neural Network model with Keras.
    """
    start_time = time.time()
    # Model building: using categorical classes
    model = width = height = None
    if model_name == "squeezeNet":
        model = SqueezeNet(weights=None, classes=n_classes)
        width = height = 227
    elif model_name == "mobileNet":
        model = MobileNet(weights=None, classes=n_classes)
        width = height = 224
    elif model_name == "inceptionV3":
        model = InceptionV3(weights=None, classes=n_classes)
        width = height = 299
    else:
        print("Wrong model name")
        exit(-1)
    # print("Using", model_name)

    # Optimizer
    op = None
    if optimizer_name == "Adadelta":
        op = Adadelta()
    elif optimizer_name == "Adam":
        op = Adam()
    elif optimizer_name == "SGD":
        op = SGD()
    else:
        print("Wrong optimizer name")
        exit(-1)
    #print("Using", optimizer_name)
    model.compile(optimizer=op, loss='categorical_crossentropy', metrics=['accuracy'])

    # Define a pre-processing process: normalize pixel values and use data augmentation.
    # Then use a Keras Generator to read images from directories in batches of fixed size. Here each image is
    # labeled with a categorical label given by the name of the directory it belongs to.
    # TODO potrebbe valere la pena investire del tempo su keras.utils.Sequence(). Generator non va bene con multiproc.
    if aug:
        train_datagen = ImageDataGenerator(rescale=1./255,
                                           width_shift_range=0.2,
                                           height_shift_range=0.2,
                                           shear_range=0.2,
                                           zoom_range=0.2,
                                           horizontal_flip=True,
                                           vertical_flip=True)
        #print("Using Data Agumentation")
    else:
        train_datagen = ImageDataGenerator(rescale=1. / 255)
        #print("NOT Using Data Agumentation")

    test_datagen = ImageDataGenerator(rescale=1. / 255)
    train_generator = train_datagen.flow_from_directory(train_dir,
                                                        target_size=(width, height),
                                                        batch_size=batch,
                                                        class_mode='categorical')
    test_generator = test_datagen.flow_from_directory(test_dir,
                                                      target_size=(width, height),
                                                      batch_size=batch,
                                                      class_mode='categorical')

    # n. of train batches evaluated (or steps) = n. of images / batch size
    train_steps = len(train_generator)
    validation_steps = len(test_generator)
    # Add more epochs in case of data augmentation
    if aug:
        # train_steps *= 5
        # validation_steps *= 5
        epochs *= 4

    # Callbacks
    if model_id is None:
        id = 'mdl'
    else:
        id = model_id
    checkpoint_id = "best_" + id + "-e{epoch:02d}-{val_loss:.2f}.hdf5"
    checkpoint = ModelCheckpoint(checkpoint_id,
                                 save_best_only=True,
                                 monitor='val_loss',
                                 mode='min')
    early_stopping = EarlyStopping(monitor='val_loss',
                                   min_delta=0,
                                   patience=3,
                                   verbose=0,
                                   mode='auto')

    # Fit the dataset
    history = model.fit_generator(train_generator,
                                  epochs=epochs,
                                  steps_per_epoch=train_steps,
                                  callbacks=[checkpoint, early_stopping],  # use early_stopping when needed
                                  validation_data=test_generator,
                                  validation_steps=validation_steps)

    print('Elapsed {} seconds'.format(time.time() - start_time))

    return history


if __name__ == '__main__':
    main()
