#!/bin/bash

# usage: create_dataset_struct.sh <patches_dir> <dest_dir>

# Creates the structure of the dataset as requested by ImageDataGenerator
# Splits dataset using specific patients IDs (listed below).

# $1 = input dir containing the patches already cropped
# $2 = output dir absolute path

input_dir=$1
output_dir=$2

# Dataset tree
echo "Creating dataset under $output_dir"
test="test"
train="train"
label1="H"
label2="AC"
label3="AD"

# Tree creation
if [ -d "$output_dir" ]; then
    echo "Output directory is not empty. Aborting."
    exit -1
fi
mkdir $output_dir

cd $output_dir
mkdir $train
mkdir $test
cd $train
mkdir $label1
mkdir $label2
mkdir $label3
cd ../$test
mkdir $label1
mkdir $label2
mkdir $label3

# First move everything to train folders. Then pick data to build test folders.
# Three classes case.
cp -R -p $(find $input_dir -maxdepth 1 -name '*H*') $output_dir/$train/$label1
cp -R -p $(find $input_dir -maxdepth 1 -name '*AC*') $output_dir/$train/$label2
cp -R -p $(find $input_dir -maxdepth 1 -name '*AD*') $output_dir/$train/$label3

# This should be equivalent to 10% of the dataset
#for i in 7 50; do
for i in 7; do
    mv $output_dir/$train/$label1/"$i"_* $output_dir/$test/$label1 
done

for i in 16; do
    mv $output_dir/$train/$label2/"$i"_* $output_dir/$test/$label2 
done

for i in 25; do
    mv $output_dir/$train/$label3/"$i"_* $output_dir/$test/$label3 
done


echo "Completed"
exit 0
