#!/bin/bash

# usage: create_dataset_struct.sh <patches_dir> <dest_dir>

# Creates the structure of the dataset as requested by ImageDataGenerator
# Splits dataset using specific patients IDs (listed below).

# $1 = input dir containing the patches already cropped
# $2 = output dir absolute path

input_dir=$1
output_dir=$2

# Dataset tree
echo "Creating dataset under $output_dir"
test="test"
train="train"
label1="healthy"
label2="sick"

# Tree creation
if [ -d "$output_dir" ]; then
    echo "Output directory is not empty. Aborting."
    exit -1
fi
mkdir $output_dir

cd $output_dir
if [ ! -d "$train" ]; then
    mkdir $train
fi
if [ ! -d "$test" ]; then
    mkdir $test
fi
cd $train
if [ ! -d "$label1" ]; then
    mkdir $label1
fi
if [ ! -d "$label2" ]; then
    mkdir $label2
fi
cd ../$test
if [ ! -d "$label1" ]; then
    mkdir $label1
fi
if [ ! -d "$label2" ]; then
    mkdir $label2
fi

# First move everything to train folders. Then pick data to build test folders.
# Two classes case.
cp -R -p $(find $input_dir -maxdepth 1 -name '*H*') $output_dir/$train/$label1
cp -R -p $(find $input_dir -maxdepth 1 -name '*AC*') $output_dir/$train/$label2
cp -R -p $(find $input_dir -maxdepth 1 -name '*AD*') $output_dir/$train/$label2

#for i in 7 50; do
for i in 7; do
    mv $output_dir/$train/$label1/"$i"_* $output_dir/$test/$label1 
done

#for i in 24 16; do
for i in 25 16; do
    mv $output_dir/$train/$label2/"$i"_* $output_dir/$test/$label2 
done

echo "Completed"
exit 0
