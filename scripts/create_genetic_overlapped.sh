#!/bin/bash

# $1 = absolute path of the folder containing .svs files
# $2 = absolute path of the output folder

out=$2
mkdir $out

# Create a folder for each patch size
for size in 2048 1024 512
do
    for overlap in 1 "3/4" "1/2"
    do
        step=$(( $size * $overlap ))
        echo "Generating size: $size, overlap: $overlap, step: $step ..."
        python ../tools/crop_images.py $1 $out/$size-$step --size $size --step $step
    done
done

exit 0
