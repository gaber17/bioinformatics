#!/bin/bash

root=/home/bioinfo_group_08/bioinformatics/genetic-patches
dest=/home/bioinfo_group_08/bioinformatics/data-genetic-3cls
for folder in `find $root -maxdepth 1 ! -path $root -type d`;do
    name=`basename $folder`
    sbatch build_dataset_3cls.sbatch $folder $dest/$name 
done
