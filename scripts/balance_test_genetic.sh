#!/bin/bash

# Balance test images in genetic dataset. Specify which dataset
# should be transformed: two or three classes.

# $1 = number of classes (2 or 3)

module load intel/python/3.5
cd /home/bioinfo_group_08/bioinformatics
source venv/bin/activate

classes=$1

if [ $classes -eq 2 ]; then
    for dir in `find data-genetic/ -maxdepth 3 -type d -name test`;do
        echo "Processing $dir"
        python tools/balance_dataset_2c.py $dir tmp
        rm -rf $dir
        mv tmp $dir
    done
elif [ $classes -eq 3 ]; then
    for dir in `find data-genetic-3cls/ -maxdepth 3 -type d -name test`;do
        echo "Processing $dir"
        python tools/balance_dataset_3c.py $dir tmp
        rm -rf $dir
        mv tmp $dir
    done
else
    echo "Class number is wrong!"
    exit -1
fi

echo "Completed"
exit 0
