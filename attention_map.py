import numpy as np
import openslide as opsl
import os
import argparse
from keras.models import load_model
from math import floor
from PIL import Image
import time


def main():
    parser = argparse.ArgumentParser(description='Generate attention maps for cancer images.')
    parser.add_argument("in_directory",
                        type=str,
                        help="Input directory. It skips any folder inside, processes only files.")
    parser.add_argument("out_directory",
                        type=str,
                        help="Output directory")
    parser.add_argument("model_file",
                        type=str,
                        help="File where model is saved")
    parser.add_argument("model_name",
                        type=str,
                        help="Can be squeezeNet or inceptionV3, depending con the CNN in model_file")
    parser.add_argument("crop_size",
                        type=int,
                        help="Crop size")
    parser.add_argument("-c", "--classes",
                        type=int,
                        default=1,
                        dest="class_index",
                        help="Index of the interested class, 0, 1 (default) or 2")
    args = parser.parse_args()
    input_dir_name = args.in_directory
    output_dir_name = args.out_directory
    model_file_name = args.model_file

    model_name = args.model_name
    model_image_size = None
    if model_name == "squeezeNet":
        model_image_size = 227
    elif model_name == "inceptionV3":
        model_image_size = 299
    else:
        print("Wrong model name")
        exit(-1)

    if not os.path.isdir(input_dir_name):
        print("Invalid input directory")
        exit(-1)

    if not os.path.exists(model_file_name):
        print("Invalid model file name")
        exit(-1)

    create_dirs(output_dir_name)

    files = [x for x in os.listdir(input_dir_name) if os.path.isfile(os.path.join(input_dir_name, x))]

    model = load_model(model_file_name)

    for n, filename in enumerate(files):
        infile = os.path.join(input_dir_name, filename)

        if filename == '.DS_Store':
            continue

        img = opsl.OpenSlide(infile)

        print("Starting elaboration of file: " + filename)
        attention_map(img, model, args.crop_size, model_image_size,
                      os.path.join(output_dir_name, filename.split(".")[0] + ".jpg"), args.class_index)


def attention_map(image, model, crop_size, resize, path, index):

    # Full resolution image dimension
    c, r = image.dimensions

    # Number of whole size patches staying in the full resolution image
    num_patch_r = floor(r / crop_size)
    num_patch_c = floor(c / crop_size)

    # Remaining pixels
    remain_r = r % crop_size
    remain_c = c % crop_size

    # Calculating and generating a black image that will contain the low resolution version of the image
    #  with the color map on it
    downsampling_factor = 16
    crop_size_downsampled = floor(crop_size / downsampling_factor)
    final_remain_r = floor(remain_r / downsampling_factor)
    final_remain_c = floor(remain_c / downsampling_factor)
    final_img = np.zeros(shape=(num_patch_r*crop_size_downsampled + final_remain_r,
                                num_patch_c*crop_size_downsampled + final_remain_c, 3),
                         dtype="uint8")

    #Main cycle to predict full patches
    i = 0
    j = 0
    start_time = time.time()
    while i < num_patch_r:
        while j < num_patch_c:
            p, to_blend_target = crop_prediction(image, j*crop_size, i*crop_size, crop_size,
                                                 resize, model, index, crop_size_downsampled)
            final_img[
                        i*crop_size_downsampled: i*crop_size_downsampled + crop_size_downsampled,
                        j*crop_size_downsampled: j*crop_size_downsampled + crop_size_downsampled,
                        :] = apply_color_map(to_blend_target, p)
            j += 1
        i += 1
        j = 0
        print("progress: " + str(floor(100 * i / num_patch_r)) +
              "%, elapsed time: " + str(floor(time.time() - start_time)) + "s")

    # Predict remaining pixels on right
    i=0
    if final_remain_c != 0:
        while i < num_patch_r:
            p, to_blend_target = crop_prediction(image, c - crop_size, i*crop_size, crop_size,
                                           resize, model, index, crop_size_downsampled)
            to_blend_target_resized = np.array(to_blend_target)[:, - final_remain_c:]
            final_img[
                        i * crop_size_downsampled: i * crop_size_downsampled + crop_size_downsampled,
                        -final_remain_c:,
                        :] = apply_color_map(to_blend_target_resized, p)
            i += 1

    i = 0

    # Predict remaining pixels on bottom
    if final_remain_r != 0:
        while i < num_patch_c:
            p, to_blend_target = crop_prediction(image, i*crop_size, r - crop_size, crop_size,
                                                 resize, model, index, crop_size_downsampled)
            to_blend_target_resized = np.array(to_blend_target)[- final_remain_r:, :]
            final_img[
                        -final_remain_r:,
                        i * crop_size_downsampled: i * crop_size_downsampled + crop_size_downsampled,
                        :] = apply_color_map(to_blend_target_resized, p)
            i+=1

    # Predict remaining pixels in bottom right corner
    if (final_remain_r != 0) and (final_remain_c!=0):
        p, to_blend_target = crop_prediction(image, c - crop_size, r - crop_size, crop_size,
                                             resize, model, index, crop_size_downsampled)
        to_blend_target_resized = np.array(to_blend_target)[ - final_remain_r:, -final_remain_c:]
        final_img[
                    -final_remain_r :,
                    -final_remain_c :,
                    :] = apply_color_map(to_blend_target_resized, p)
    Image.fromarray(final_img, "RGB").save(path)


def create_dirs(path):
    if not os.path.exists(path):
        os.makedirs(path)


def crop_prediction(image, j, i, crop_size, resize_predict, model, index, resize_downsample):
    '''
    Predict a the probability of a patch of belonging to the interested class
    '''
    to_predict = image.read_region((j, i), 0, (crop_size, crop_size))
    to_predict = to_predict.convert("RGB")
    to_show = np.array(to_predict.resize((resize_predict, resize_predict), Image.BILINEAR)) / 255
    return model.predict(np.array([to_show]))[0][index], np.array(to_predict.resize((resize_downsample, resize_downsample)))


def apply_color_map(target, p):
    """
    Generating a red image that will be blended with the original downsampled image basing on class probability.
    """
    to_blend = np.zeros(shape=target.shape, dtype="uint8")
    to_blend[:, :, 0] = 255

    alfa = floor(p*5)
    alfa = alfa /10
    return (1 - alfa)*target + alfa*to_blend


if __name__ == '__main__':
    main()
