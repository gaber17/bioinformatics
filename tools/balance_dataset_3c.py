from balance_dataset_2c import read_path_images
import argparse
import os
from random import sample
from shutil import copyfile

sep = os.sep

def main():
    parser = argparse.ArgumentParser(description="Balance the number of input images among all labels.")
    parser.add_argument("input_dir",type=str, help="Input directory with unbalanced data")
    parser.add_argument("output_dir", type=str, help="Output directory for balanced data")
    args = parser.parse_args()
    input_dir = args.input_dir
    output_dir = args.output_dir
    balance_three_classes(input_dir, output_dir)


def balance_three_classes(input_path, output_path):
    """
    Uniformly distribute class labels ('AC', 'AD', 'H') among patches within input directory.
    """
    images, labels = read_path_images(input_path)

    if not os.path.exists(output_path + sep + "AC"):
        os.makedirs(output_path + sep + "AC")
    if not os.path.exists(output_path + sep + "H"):
        os.makedirs(output_path + sep + "H")
    if not os.path.exists(output_path + sep + "AD"):
        os.makedirs(output_path + sep + "AD")

    ac = []
    ad = []
    h = []
    ac_count = 0
    ad_count = 0
    h_count = 0

    if len(images) != len(labels):
        raise RuntimeError("Images and their labels are not matching")

    for i in range(len(images)):

        if labels[i] == 1:
            ac.append(images[i])
            ac_count += 1
        elif labels[i] == 2:
            ad.append(images[i])
            ad_count += 1
        else:
            h.append(images[i])
            h_count += 1

    max_img_per_lab = min(ac_count, ad_count, h_count)

    hs = sample(h, max_img_per_lab)
    ads = sample(ad, max_img_per_lab)
    acs = sample(ac, max_img_per_lab)

    for image in hs:
        copyfile(image,
                 output_path + sep + "H" + sep + image.split(sep)[- 1])
    for image in acs:
        copyfile(image,
                 output_path + sep + "AC" + sep + image.split(sep)[- 1])
    for image in ads:
        copyfile(image,
                 output_path + sep + "AD" + sep + image.split(sep)[- 1])

if __name__ == '__main__':
        main()
