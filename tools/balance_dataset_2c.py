from PIL import Image
import numpy
import os
import fnmatch
from shutil import copyfile
from random import sample
import argparse

sep = os.sep


def main():
    parser = argparse.ArgumentParser(description="Balance the number of input images among all labels.")
    parser.add_argument("input_dir",type=str, help="Input directory with unbalanced data")
    parser.add_argument("output_dir", type=str, help="Output directory for balanced data")
    args = parser.parse_args()
    input_dir = args.input_dir
    output_dir = args.output_dir
    balance_two_classes(input_dir, output_dir)


def read_path_images(path):

    images = []
    labels = []
    for filename in os.listdir(path):

        if filename == '.DS_Store':
            continue

        current_path = os.path.join(path, filename)
        if os.path.isdir(current_path):
            (newImages, newLabels) = read_path_images(current_path)
            images.extend(newImages)
            labels.extend(newLabels)
        else:
            if fnmatch.fnmatch(filename, "*H*"):
                labels.append(0)
            elif fnmatch.fnmatch(filename, "*AC*"):
                labels.append(1)
            elif fnmatch.fnmatch(filename, "*AD*"):
                labels.append(2)
            else:
                raise RuntimeError('Wrong format for image: {}'.format(current_path))
            images.append(current_path)

    return images, labels


def balance_two_classes(input_path, output_path):
    """
    Uniformly distribute class labels ('Healthy', 'Sick') among patches within input directory.
    """

    images, labels = read_path_images(input_path)

    if not os.path.exists(output_path + sep + "sick"):
        os.makedirs(output_path + sep + "sick")
    if not os.path.exists(output_path + sep + "healthy"):
        os.makedirs(output_path + sep + "healthy")

    ac = []
    ad = []
    h = []
    ac_count = 0
    ad_count = 0
    healthy_count = 0

    if len(images) != len(labels):
        raise RuntimeError("Images and their labels are not matching")

    for i in range(len(images)):

        if labels[i] == 1:
            ac.append(images[i])
            ac_count+=1
        elif labels[i] == 2:
            ad.append(images[i])
            ad_count+=1
        else:
            h.append(images[i])
            healthy_count+=1

    sick_count = ac_count + ad_count

    if sick_count <= healthy_count:
        maxImages = sick_count
        hs = sample(h, maxImages)
        for image in hs:
            copyfile(image,
                     output_path + sep + "healthy" + sep + image.split(sep)[- 1])
        for image in ac:
            copyfile(image,
                     output_path + sep + "sick" + sep + image.split(sep)[- 1])
        for image in ad:
            copyfile(image,
                     output_path + sep + "sick" + sep + image.split(sep)[- 1])

    else:
        maxImages = healthy_count
        max_sick = int(maxImages / 2)
        if max_sick <= ac_count and max_sick <= ad_count:
            acs = sample(ac, max_sick)
            ads = sample(ad, max_sick)
        elif ac_count < max_sick:
            acs = sample(ac, ac_count)
            ads = sample(ad, maxImages - ac_count)
        elif ad_count < max_sick:
            ads = sample(ad, ad_count)
            acs = sample(ac, maxImages - ad_count)

        for image in h:
            copyfile(image,
                     output_path + sep + "healthy" + sep + image.split(sep)[- 1])
        for image in acs:
            copyfile(image,
                     output_path + sep + "sick" + sep + image.split(sep)[- 1])
        for image in ads:
            copyfile(image,
                     output_path + sep + "sick" + sep + image.split(sep)[- 1])


if __name__ == '__main__':
    main()
