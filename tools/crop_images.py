import openslide as opsl
from openslide.deepzoom import DeepZoomGenerator
import os as os
import shutil
import argparse
import time


def main():
    start_time = time.time()
    parser = argparse.ArgumentParser(description='Crop whole slide images.')
    parser.add_argument("in_directory",
                        type=str,
                        help="Input directory. It skips any folder inside, processes only files.")
    parser.add_argument("out_directory",
                        type=str,
                        help="Output directory")
    parser.add_argument("-f", "--format",
                        action="store",
                        dest="format",
                        type=str,
                        default="jpg",
                        help="Format for the cropped images (default: jpg)")
    parser.add_argument("-s", "--size",
                        action="store",
                        dest="size",
                        type=int,
                        default=2048,
                        help="Crop size (default: 2048)")
    parser.add_argument("--step",
                        action="store",
                        dest="step",
                        type=int,
                        default=0,
                        help="Step between two patches (default equivalent to size)")
    parser.add_argument("-a", "--all",
                        action="store_true",
                        help="Generate the patches for all the crop sizes [ 256 to 2048 ]")
    args = parser.parse_args()
    if args.step == 0:
        step = args.size
    else:
        step = args.step
    if args.all:
        create_all(args.in_directory, args.out_directory, args.format, step)
    else:
        crop_images(args.in_directory,
                    args.out_directory,
                    args.format,
                    args.size,
                    step)
    print('Elapsed {:d} seconds'.format(int(time.time() - start_time)))


def create_dirs(path):
    if not os.path.exists(path):
        os.makedirs(path)


def crop_images(input_dir_name, output_dir_name, output_format, crop_size, step):
    """
    Crop WSI images and create a folder for each patient's data, containing the patches
    """

    if not os.path.isdir(input_dir_name):
        print("Invalid input directory")
        exit(-1)
    create_dirs(output_dir_name)

    # excluding any directory in input directory
    files = [x for x in os.listdir(input_dir_name) if os.path.isfile(os.path.join(input_dir_name, x))]
    file_count = len(files)

    for n, filename in enumerate(files):
        if filename == '.DS_Store':
            continue
        infile = os.path.join(input_dir_name, filename)
        out_file_prefix = filename.split(".")[0]
        out_dir_path = os.path.join(output_dir_name, out_file_prefix)
        create_dirs(out_dir_path)

        crop_image(infile, crop_size, step, out_dir_path, out_file_prefix, output_format, 0.25)

        print('\r{:d} %'.format(int((n + 1) / file_count * 100)), end='')


def take_crop(obj, crop_size, i, j, out_prefix, out_suffix, out_format):
    img = obj.read_region((i, j), 0, (crop_size, crop_size))
    img = img.convert('RGB')  # RGBA -> RGB
    img.save('{}_{}.{}'.format(out_prefix, out_suffix, out_format))


def crop_image(infile, crop_size, step, out_dir_path, out_file_prefix, output_format, margin_threshold):

    obj = opsl.OpenSlide(infile)

    out_path_prefix = os.path.join(out_dir_path, '{}_crop'.format(out_file_prefix))

    threshold = margin_threshold * crop_size

    # Version 1
    r, c = obj.dimensions
    i = 0
    j = 0
    num = 0

    #Cropping the image with a dimension of crop_size leads to a remaining part of the image of size <crop_size
    #this evaluation is needed to understand if this portion is significative with respect to crop size
    #in this case an overlapped patch is taken
    crop_border_r = r % crop_size >= threshold
    crop_border_c = c % crop_size >= threshold

    while i + crop_size < r:
        while j + crop_size < c:
            take_crop(obj, crop_size, i, j, out_path_prefix, str(num), output_format)
            num += 1
            j += step
        if crop_border_c:
            take_crop(obj, crop_size, i, c - crop_size, out_path_prefix, str(num), output_format)
            num += 1
        j = 0
        i += step
    if crop_border_r:
        while j + crop_size < c:
            take_crop(obj, crop_size, r - crop_size, j, out_path_prefix, str(num), output_format)
            num += 1
            j += step
        if crop_border_c:
            take_crop(obj, crop_size, r - crop_size, c - crop_size, out_path_prefix, str(num), output_format)




def empty_folder(folder):
    for _file in os.listdir(folder):
        file_path = os.path.join(folder, _file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print(e)


def create_all(input_dir, output_dir, output_format, step):
    """
    Create patches of all sizes [2048, 1024, 512, 256, 128]
    """
    sizes = [2048, 1024, 512, 256, 128]
    home = "/home/bioinfo_group_08/"

    empty_folder(output_dir)
    for s in sizes:
        print('Creating patches of size {}'.format(s))
        folder = '{}x{}'.format(s, s)
        out_path = os.path.join(output_dir, folder)
        crop_images(input_dir, out_path, output_format, s, step)


if __name__ == '__main__':
    main()
