### Code

Limit the number of cores used in Keras with Tensorflow backend:
```python
from keras import backend as K
K.set_session(K.tf.Session(config=K.tf.ConfigProto(intra_op_‌​parallelism_threads=‌​32, inter_op_parallelism_threads=32)))
```