from random import Random
from random import random
from time import time
import numpy as np
import inspyred
import time
import argparse
import os
import sys
from numpy import random as rn
import build_model as bm


optimizersName = ['Adam', 'Adadelta', 'SGD']
optimizers = [0, 1, 2]  # 0 = Adam, 1 = Adadelta, 2 = SGD
overlapTypesNames = [0, 0.25, 0.5]
overlapTypes = [0,1,2]
patchSizeNames = [2048, 1024, 512]
patchSize = [0,1,2]
dataAugmentationNames = ['No-data-augmentation', 'Data-augmentation']   # 0 = no data-aug, 1 = do use data-aug
dataAugmentation = [0,1]

RESULT_FILE = "genetic_results.txt"
CACHE_FILE = "genetic_prev_results.txt"

VALID_MODELS = ['squeezeNet', 'inceptionV3']
VALID_CLASSES = [2, 3]
DATASET_DIR = None
CLASSES = None
MODEL = None

def main(prng=None, display=False):
    """
    Create=a human-readable file to save candidates properties and how they performed.
    The candidates have the following properties:
    - Optimizer: Adam, Adadelta, SGD
    - Overlap-type: 0, 0.25, 0.5
    - Patch-size: 512, 1024, 2048
    - Data-augmentation: Y, N
    """

    parser = argparse.ArgumentParser(description='Launch genetic algorithm for training.')
    parser.add_argument("dataset_dir",
                        type=str,
                        help="Dataset directory.")
    parser.add_argument("classes",
                        type=int,
                        help="Number of classes: 2 or 3")
    parser.add_argument("model",
                        type=str,
                        help="Which model should be used: squeezeNet or inceptionV3")
    args = parser.parse_args()
    if args.model not in VALID_MODELS or args.classes not in VALID_CLASSES:
        print("Unknow model or class number")
        sys.exit(-1)

    global DATASET_DIR
    DATASET_DIR = args.dataset_dir
    global CLASSES
    CLASSES = args.classes
    global MODEL
    MODEL = args.model

    _format = 'Format: optimizer, overlap-type, patch-size, data-augmentation, acc, loss, val_acc, val_loss'
    with open(RESULT_FILE, "w") as fp:
        fp.write('# {} # {} \n'.format(args.model, _format))

    #TODO: forse dovrebbe essere sempre lo stesso per una questione di repricabilità degli esperimenti?
    if prng is None:
        prng = Random()
        prng.seed(1)

    ea = inspyred.ec.GA(prng)
    ea.terminator = inspyred.ec.terminators.evaluation_termination

    ea.variator[0] = inspyred.ec.variators.uniform_crossover
    ea.variator[1] = inspyred.ec.variators.gaussian_mutation


    final_pop = ea.evolve(generator=custom_generator,
                          evaluator=custom_evaluator,
                          bounder=custom_bounder,
                          pop_size=6,
                          maximize=False,
                          max_evaluations=30,
                          mutation_rate=0.4,
                          num_elites=1)

    if display:
        best = min(final_pop)
        print('Best Solution: \n{0}'.format(str(best)))


def return_closest(value_set, n):
    return min(value_set, key=lambda x:abs(x-n))

def load_cached_results():

    individuals = dict()
    with open(CACHE_FILE, "r") as fp:
        lines = fp.readlines()
        for i in range(1, lines.__len__()):
            line = lines[i]
            tokens = line.split(",")
            op = optimizersName.index(tokens[0])
            ovType = overlapTypesNames.index(float(tokens[2]))
            patch = patchSizeNames.index(float(tokens[1]))
            aug = dataAugmentationNames.index(tokens[3])
            id = str(optimizersName[op]) + "-" + str(patchSizeNames[patch]) + "-" + str(
                overlapTypesNames[ovType]) + "-" + str(dataAugmentationNames[aug])
            if id not in individuals:
                acc = float(tokens[3 + 1])
                loss = float(tokens[3 + 2])
                val_acc = float(tokens[3 + 3])
                val_loss = float(tokens[3 + 4])
                individuals[id] = [acc, loss, val_acc, val_loss]
    return individuals


def custom_bounder(candidate, args):

    """
    This function bounds the candidates, modified by the genetic operators,
    so that they do not exceed above their limits.
    """
    individuals = args.get('individuals', dict())

    op = return_closest(optimizers, candidate[0])
    ovType = return_closest(overlapTypes, candidate[1])
    patch = return_closest(patchSize, candidate[2])
    aug = return_closest(dataAugmentation, candidate[3])

    id = str(optimizersName[op]) + "-" + str(patchSizeNames[patch]) + "-" + str(
        overlapTypesNames[ovType]) + "-" + str(dataAugmentationNames[aug])

    if id not in individuals:
        individuals[id] = [0,0,0,0]
        args['individuals'] = individuals

    list = [op, ovType, patch, aug]

    return list


def custom_generator(random, args):

    """
    This function randomly generates the individuals that will make up the population,
    according to the default values commonly used for each hyper-parameter.
    """

    individuals = args.get('individuals', dict())
    
    while True:

        op = random.choice(optimizers)
        ovType = random.choice(overlapTypes)
        patch = random.choice(patchSize)
        aug = random.choice(dataAugmentation)
        list = [op, ovType, patch, aug]
        id = str(optimizersName[op]) + "-" + str(patchSizeNames[patch]) + "-" + str(
            overlapTypesNames[ovType]) + "-" + str(dataAugmentationNames[aug])
        if id not in individuals:
            individuals[id] = [0,0,0,0]
            args['individuals'] = individuals
            break

    return list


def custom_evaluator(candidates, args):

    """
    This function is the core of the algorithm: it trains and test each neural network
    which is uniquely identified by each list of genes - i.e. the hyper-parameters and
    evaluate them according to the accuracy shown on the validation set.
    """

    start_time = time.time()
    
    if not args.get('alreadyGenerated'):
        if os.path.isfile(CACHE_FILE):
            print("Found cached results loading them...")
            args['alreadyGenerated'] = load_cached_results()
        else:
            print("Cached models not found")
            args['alreadyGenerated'] = {}

    fitness = []
    for cs in candidates:
        op = cs[0]
        ovType = cs[1]
        patch = cs[2]
        aug = cs[3]

        id = str(optimizersName[op]) + "-" + str(patchSizeNames[patch]) +\
             "-" + str(overlapTypesNames[ovType]) + "-" + str(dataAugmentationNames[aug])
        cds = str(optimizersName[op]) + "," + str(patchSizeNames[patch]) \
              + "," + str(overlapTypesNames[ovType]) + "," + str(dataAugmentationNames[aug])

        # e.g. patchSize=2048, ovType=0.25 -> step=1536, coherent with crop_size.py notation
        step = int(patchSizeNames[patch] - patchSizeNames[patch] * overlapTypesNames[ovType])

        main_dir = os.path.join(DATASET_DIR, str(patchSizeNames[patch]) + "-" + str(step))
        train_dir = os.path.join(main_dir, "train")
        test_dir = os.path.join(main_dir, "test")

        print('Starting model id: {}'.format(id))
        alreadyGen = args.get('alreadyGenerated')
        individuals = args.get('individuals')
        if id in alreadyGen:
            val_categorical_accuracy = alreadyGen[id][2]
            val_loss = alreadyGen[id][3]
            acc = alreadyGen[id][0]
            loss = alreadyGen[id][1]

        elif (id in individuals) and (individuals[id][0] != 0):
            val_categorical_accuracy = individuals[id][2]
            val_loss = individuals[id][3]
            acc = individuals[id][0]
            loss = individuals[id][1]

        else:

            #Training configuration is fixed. See build_model.py
            history = bm.train(train_dir, test_dir,
                              epochs=10,
                              batch=32,
                              model_name=MODEL,
                              optimizer_name=optimizersName[op],
                              aug=aug,
                              n_classes=CLASSES,
                              model_id=id)

            # best_index should correspond to MIN used by EarlyStopping to stop the training
            best_index = np.argmin(history.history['val_loss'])
            val_loss = history.history['val_loss'][best_index]
            acc = history.history['acc'][best_index]
            loss = history.history['loss'][best_index]
            val_categorical_accuracy = history.history['val_acc'][best_index]
            # val_loss = random()
            # val_categorical_accuracy = random()
            individuals[id][0] = acc
            individuals[id][1] = loss
            individuals[id][2] = val_categorical_accuracy
            individuals[id][3] = val_loss
            args['individuals'] = individuals


        # fitness += [val_categorical_accuracy]
        fitness += [val_loss]

        print('Finished model id: {}'.format(id) + ' - '+str(val_categorical_accuracy)+ '- '+ str(val_loss))

        with open(RESULT_FILE, "a") as fp:
            line = cds + "," + str(acc) + "," + str(loss) + "," + str(val_categorical_accuracy) + "," + str(val_loss) + "\n"
            fp.write(line)

    print(' ---- Elapsed seconds: {:.2f} ---- '.format(time.time() - start_time))
    return fitness


if __name__ == '__main__':
    main(display=True)
